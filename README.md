This is our "Graphisme et visualisation" project for Polytech'Paris-Sud. The project's code name is **flappysing**.
The aim is to use __Processing__ to create a flappy-like game where you control the bird's height thanks to the pitch of your voice.

The project require the audio library **Minim** : http://code.compartmental.net/minim/