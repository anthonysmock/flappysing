class Oiseau
{
    float gravity = .3; 
    float OiseauX, OiseauY;
  
    float vie = 100;
    int maxQuantiteVie = 100;
    int vieEnleve = 1;
    int BarreVie = 60;
  
    float OiseauTaille=70;
    PImage img; 
    int compteur = 0;
    int indice=0;
    int maxTempsBattement=10;
    
  import sprites.*;
  void dessinBoule()
  {
    color Oiseau=color(0, 51,102);
    fill(Oiseau);
    tempsBattement++;
  
    if(tempsBattement>=maxTempsBattement)
    {
      score++;
      tempsBattement=0;
      indice = frameCount%sprites.length;
    }
    PImage p = sprites[indice];
    image( p , 
    oiseau.OiseauX, oiseau.OiseauY ,
    oiseau.OiseauTaille , oiseau.OiseauTaille );
    
  }
  
  // forcer  pour qu il reste dans l ecran
  void forcerEcran() 
  {
    if (oiseau.OiseauY+(oiseau.OiseauTaille/2) > height) 
    { 
      deplacerBas(height);      //  La balle touche le plafond donc la descendre
    }

    if (oiseau.OiseauY- ( oiseau.OiseauTaille/2 ) < 0) 
    {
      deplacerHaut(0);    //  La balle touche le sol donc la remonter
    }
  }
  
  // Descendre la balle
  void deplacerBas(float surface) 
  {
    oiseau.OiseauY = surface- ( oiseau.OiseauTaille );
  }
  
  // Remonter la balle
  void deplacerHaut(float surface) 
  {
    oiseau.OiseauY = surface + ( oiseau.OiseauTaille/2 );
  }
  
  void gravitation() 
  {
  oiseau.OiseauY += 0.5;
  }

  void defaite()
  {
    ecran=2;
  }

  void rejouer() 
  {
    score = 0;
    vie = maxQuantiteVie;
    OiseauX=width/4;
    OiseauY=height/5;
    temps = 0;
    list_obstacle.clear();
    ecran = 0;
  }
  void commencer_jeu() 
  {
    ecran=1;
  }
  
  void barreVie() 
  {
    fill(190, 205, 200);
    int a=2,b=44,c=5;
    rect(OiseauX - ( BarreVie / a ),  
    OiseauY - b,  
    BarreVie,  
    c );
    if (vie > 60) fill(49, 210, 105);
    else if (vie > 30) fill(229, 130, 40);
    else fill(229, 80, 59);
    
    rect( OiseauX - ( BarreVie / a ) ,  
    OiseauY - b, 
    BarreVie * ( vie / maxQuantiteVie ), 
    c );
  }
  
  void Enlevevie() 
  {
    explosion.setXY(OiseauX, OiseauY);
    explosion.setFrameSequence(0, 24, 0.02, 1);
    vie -= vieEnleve;
    if (vie <= 0) 
    {
      defaite();
    }
  }
  
  void augmenteScore() 
  {
    score++;
  }
  
  void afficheAugmenteScore() 
  {
    textAlign(CENTER);
    fill(0);
    textSize(30); 
    text(score+ " m", width/2, 50);
  }
}