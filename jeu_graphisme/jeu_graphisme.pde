//audio
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.*;

import sprites.*;
import sprites.maths.*;
import sprites.utils.*;

int obsLargeur = 0;
boolean place = false;
PImage backgnd, montagnes,nuages; 
int xmontagnes=0,xmontagnesbis, xnuages=0,xnuagesbis, y=1, t; 
Sprite explosion;
PImage sp;
PImage oiseauVolant;
int DIM = 4;
PImage[] sprites = new PImage[DIM];
// This will be used for timing
StopWatch sw = new StopWatch();


int espace = 21;
boolean augmente = false;

PitchDetectorFFT PD;
//PitchDetectorAutocorrelation PD;
ToneGenerator TG;
AudioSource AS;
Minim minim;

int tempsBattement = 100;

float temps=-5000;
float tempsJeu =5000;
//int minheight = 200;
//int maxheight = 300;

ArrayList<int[]> list_obstacle = new ArrayList<int[]>();
Oiseau oiseau = new Oiseau();
Obstacle obstacle = new Obstacle();
int ecran = 0;
int score=0; 

float moy = 0;
float aigue=0;
float grave=0;

float commence_temps = -1;
int cpt_etal=0;
int nb_freq = 0;//10; //100

float min = 700; //700
float max = 900; //900
  

//Creation de la fenetre
void setup()
{
  //size(600, 500);
  //surface.setResizable(true);
  fullScreen();
  montagnes=loadImage("montagnes.png");
  xmontagnesbis=montagnes.width;
  nuages=loadImage("nuages.png");
  xnuagesbis=nuages.width; 
  oiseau.OiseauX=width/4;
  oiseau.OiseauY=height/5;
  oiseau.img = loadImage("a.png");
  
  backgnd=loadImage("paysage.png"); 
  backgnd.resize(width,height);

  smooth();
  
  minim = new Minim(this);
  AS = new AudioSource(minim);
  AS.OpenMicrophone();
  PD = new PitchDetectorFFT();
  PD.ConfigureFFT(2048, AS.GetSampleRate());
  PD.SetSampleRate(AS.GetSampleRate());
  AS.SetListener(PD);
  
  explosion = new Sprite(this, "explosion.png", 5, 5, 10);
  oiseauVolant = loadImage("c.png");
  int W = oiseauVolant.width/5; 
  int H = oiseauVolant.height/3;
  for(int i=0;i<DIM;i++)
  {
    int x =  i%5*W;
    int y =  i%3*H;
    sprites[i] = oiseauVolant.get(x, y, W, H);
  }
}

//Remplissage de la fenetre
void draw()
{
  if (ecran == 0) ecran_initial();
  else if (ecran == 1) ecran_jeu(); 
  else if (ecran == 2) ecran_defaite(); 
  
    // Get the elapsed time since the last frame
  float elapsedTime = (float) sw.getElapsedTime();
  S4P.updateSprites(elapsedTime);
  S4P.drawSprites();
}

void mise_en_forme_texte_ecran()
{
  background(44, 62, 80);
  textAlign(CENTER);
  fill(236, 240, 241);
}

void ecran_defaite() {
  mise_en_forme_texte_ecran();  
  textSize(40); text("Score obtenu", width/2, height/2 - 130);
  textSize(90); text(score+ " m", width/2, height/2);
  textSize(16); text("Cliquez pour rejouer", width/2, height-40);
}

public void mousePressed() 
{
  if (ecran==0) 
  { 
    oiseau.commencer_jeu();
  }
  if (ecran==2) 
  {
    espace = 21;
    oiseau.rejouer();
  }
}

void ecran_initial() 
{
  background(236, 240, 241);
  textAlign(CENTER);
  fill(52, 73, 94);
  textSize(70); text("Flappy Sing", width/2, height/2);
  textSize(15); text("Cliquez pour commencer", width/2, height-30);
}

void ecran_jeu()
{
  imageMode(CORNER);
  dessiner_paysage();  
  dessiner_montagnes();
  dessiner_nuages();
  bouger();
  //background(236, 240, 241);
  oiseau.dessinBoule();
  oiseau.gravitation();
  oiseau.forcerEcran();
  
  if(score % 25 == 0 && espace > 2)
  {
    if(!augmente)
    {
      espace--;
      augmente = true;
    }
  } else {
    augmente = false;
  }
  
  if(score % espace == 0)
  {
    if(!place)
    {
      obstacle.ajoutObstacle();
      place = true;
    }
  } else {
    place = false;
  }
  
  obstacle.gestionObstacle();
  
  oiseau.barreVie();
  oiseau.afficheAugmenteScore();
  
  if (commence_temps == -1)
  commence_temps = millis();

  float f = 0;
  /*float level = AS.GetLevel();
  long t = PD.GetTime();
  if (t == last_t) return;
  last_t = t;*/

  f = PD.GetFrequency();
  if(nb_freq==0)
  {
    aigue=790;
    grave=950;
  }
  //TG.SetFrequency(f);
  //TG.SetLevel(level * 10.0);
  moy=(aigue+grave)/2;
  float intervalle = 10;
  if(f >=aigue-aigue/intervalle && f<=aigue+aigue/intervalle) //aigu il monte
  {
    oiseau.OiseauY += f/80; //height - ((f-min)/(max-min))*height;
   // print(aigue + "\n");
     print("freq: "+ f+" aigu: " +aigue+ " grave: "+grave+"\n");
  }
  else if(f >=grave-grave/intervalle && f<=grave+grave/intervalle) //grave il descend
  {
    oiseau.OiseauY -= f/80; //height - ((f-min)/(max-min))*height;
    //print(grave + "\n");
     print("freq: "+ f+" aigu: " +aigue+ " grave: "+grave+"\n");
  }
}

void dessiner_montagnes() 
{   
  image(montagnes, xmontagnes,height-montagnes.height);
  image(montagnes, xmontagnesbis, height-montagnes.height);
} 

void dessiner_nuages()
{
  image(nuages, xnuages, 0);
  image(nuages, xnuagesbis, 0); 
}

void dessiner_paysage()
{ 
  image(backgnd, 0, 0);
  //background(backgnd);
  
} 
void bouger()
{ 
  xmontagnes-=4;
  xmontagnesbis-=4;
  xnuages-=1;
  xnuagesbis-=1;
  
  if(xmontagnes<=-montagnes.width)
  {
    xmontagnes=montagnes.width;
  }
  if(xmontagnesbis<=-montagnes.width)
  {
    xmontagnesbis=montagnes.width;
  }
  
  if(xnuages<=-nuages.width)
  {
    xnuages=nuages.width;
  }
  if(xnuagesbis<=-nuages.width)
  {
    xnuagesbis=nuages.width;
  }
  
} 