PImage backgnd, montagnes,nuages; 
int xmontagnes=0,xmontagnesbis, xnuages=0,xnuagesbis, y=1, t; 

void setup() { 
  size(1024, 768); 
  montagnes=loadImage("montagnes.png");
  xmontagnesbis=montagnes.width;
  nuages=loadImage("nuages.png");
  xnuagesbis=nuages.width; 
} 

void draw() { 
nettoyer(); 
dessiner_montagnes();
dessiner_nuages();
bouger(); 
} 

void dessiner_montagnes() 
{ 
  image(montagnes, xmontagnes, height-montagnes.height); 
  image(montagnes, xmontagnesbis, height-montagnes.height); 
} 

void dessiner_nuages()
{
  image(nuages, xnuages, 0);
  image(nuages, xnuagesbis, 0); 
}

void nettoyer()
{ 
  backgnd=loadImage("paysage.png"); 
  image(backgnd, 0, 0); 
} 

void bouger()
{ 
  xmontagnes-=10;
  xmontagnesbis-=10;
  xnuages-=2;
  xnuagesbis-=2;
  
  if(xmontagnes<=-montagnes.width)
  {
    xmontagnes=montagnes.width;
  }
  if(xmontagnesbis<=-montagnes.width)
  {
    xmontagnesbis=montagnes.width;
  }
  
  if(xnuages<=-nuages.width)
  {
    xnuages=nuages.width;
  }
  if(xnuagesbis<=-nuages.width)
  {
    xnuagesbis=nuages.width;
  }
  
} 