class Obstacle
{
  void ajoutObstacle()
  {
    int obstaclewidth = width/20;
    int randheight = round(random(300, 500)); //200 300
    
    //if(millis()-temps>tempsJeu)
    {
        int randY = round(random(0, height-randheight));
        int[] obstacle = {width, randY, obstaclewidth, randheight, 0};
        obsLargeur++;
        list_obstacle.add(obstacle); 
        temps = millis();
    }
  }
  
  void gestionObstacle()
  {
    for(int i=0;i<list_obstacle.size();i++)
    {
      EnleveObstacle(i);
      if(list_obstacle.size()!=0)
      {
        BougerObstacle(i);
        DessinObstacle(i);
        gestionObstacleCollision(i);
      }
    }
  }
  
  void gestionObstacleCollision(int index) {
  int[] obstacle = list_obstacle.get(index);
  // get position_ obstacle settings 
  int position_obstacleX = obstacle[0];
  int position_obstacleY = obstacle[1];
  int position_obstaclewidth = obstacle[2];
  int position_obstacleheight = obstacle[3];
  int obstacleScored = obstacle[4];
  int obstacleUnX = position_obstacleX;
  int obstacleUnY = 0;
  int obstacleUnwidth = position_obstaclewidth;
  int obstacleUnheight = position_obstacleY;
  int obstacleDeuxX = position_obstacleX;
  int obstacleDeuxY = position_obstacleY+position_obstacleheight;
  int obstacleDeuxwidth = position_obstaclewidth;
  int obstacleDeuxheight = height-(position_obstacleY+position_obstacleheight);

  if (
    ((oiseau.OiseauX+(oiseau.OiseauTaille/2)>obstacleUnX) &&
    (oiseau.OiseauX-(oiseau.OiseauTaille/2)<obstacleUnX+obstacleUnwidth) &&
    (oiseau.OiseauY+(oiseau.OiseauTaille/2)>obstacleUnY) &&
    (oiseau.OiseauY-(oiseau.OiseauTaille/2)<obstacleUnY+obstacleUnheight))
    || (
        (oiseau.OiseauX+(oiseau.OiseauTaille/2)>obstacleDeuxX) &&
    (oiseau.OiseauX-(oiseau.OiseauTaille/2)<obstacleDeuxX+obstacleDeuxwidth) &&
    (oiseau.OiseauY+(oiseau.OiseauTaille/2)>obstacleDeuxY) &&
    (oiseau.OiseauY-(oiseau.OiseauTaille/2)<obstacleDeuxY+obstacleDeuxheight)
    )
    ) 
  {
    oiseau.Enlevevie();
    //obstacle[0] = 0;
    //obstacle[2] = 0;
    //EnleveObstacle(index);
  }

  if (oiseau.OiseauX > position_obstacleX+(position_obstaclewidth/2) && obstacleScored==0) 
  {
    obstacleScored=1;
    obstacle[4]=1;
    oiseau.augmenteScore();
  }
}
  
  
  void DessinObstacle(int index)
  {
    color obstacleCouleur = color(144, 64, 82);
  
    int[] obstacle = list_obstacle.get(index);
    // get position_ obstacle settings 
    int position_obstacleX = obstacle[0];
    int position_obstacleY = obstacle[1];
    int position_obstaclewidth = obstacle[2];
    int position_obstacleheight = obstacle[3];
    //dessiner les list_obstacle
    rectMode(CORNER);
    noStroke();
    strokeCap(ROUND);
    fill(obstacleCouleur);
    int couleur1 = 15;
    int couleur2 = 0;
    rect(position_obstacleX, 0, position_obstaclewidth, position_obstacleY, couleur2, couleur2, couleur1, couleur1);
    rect(position_obstacleX, position_obstacleY+position_obstacleheight, position_obstaclewidth, 
          height-(position_obstacleY+position_obstacleheight), couleur1, couleur1, couleur2, couleur2);
  }

  void BougerObstacle(int index) 
  {
    int obstacleVitesse = 5;
    int[] obstacle = list_obstacle.get(index);
    obstacle[0] -= obstacleVitesse;
  }
  
  void EnleveObstacle(int index) 
  {
    int[] obstacle = list_obstacle.get(index);
    if (obstacle[0]+obstacle[2] <= 0) 
    {
      list_obstacle.remove(index);
    }
  }
}